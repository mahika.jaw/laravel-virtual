<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice_item extends Model
{
    use HasFactory;
     
    protected $fillable = [
        'invoice_id', 'product_id', 'quantity', 'cost_price', 'total_price', 'created_by', 'updated_at', 'deleted_at', 'deleted_by'
    ];

    public function products(){
        return $this->hasOne(Product::class, 'id', 'product_id');
    }
}
