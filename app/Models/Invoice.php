<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Invoice_item;
class Invoice extends Model
{
    use HasFactory;
     
    protected $fillable = [
     'customer_id', 'invoice_date', 'invoice_number', 'total_value', 'created_at', 'created_by', 'updated_at', 'deleted_at'
    ];

    public function invoice_items(){
        return $this->hasMany(Invoice_item::class,'invoice_id','id');
    }

    public function customer(){
        return $this->hasOne(Customer::class, 'id', 'customer_id');
    }
}
