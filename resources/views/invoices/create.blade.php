@extends('layouts.main')
  
@section('body')
<style type="text/css">
.parsley-errors-list { 
list-style-type: none
}

.parsley-custom-error-message  {
color: red;
margin-top: 5px;
/* margin-left: -40px; */
}


input.parsley-success,
       select.parsley-success,
       textarea.parsley-success {
         color: #468847;
         background-color: #DFF0D8;
         border: 1px solid #D6E9C6;
       }

       input.parsley-error,
       select.parsley-error,
       textarea.parsley-error {
         color: #B94A48;
         background-color: #F2DEDE;
         border: 1px solid #EED3D7;
       }

       .parsley-errors-list {
         margin: 2px 0 3px;
         padding: 0;
         list-style-type: none;
         font-size: 0.9em;
         line-height: 0.9em;
         opacity: 0;

         transition: all .3s ease-in;
         -o-transition: all .3s ease-in;
         -moz-transition: all .3s ease-in;
         -webkit-transition: all .3s ease-in;
       }

       .parsley-errors-list.filled {
         opacity: 1;
       }

.parsley-type,
       .parsley-required,
       .parsley-equalto,
       .parsley-pattern,
       .parsley-urlstrict,
       .parsley-length,
       .parsley-checkemail{
        color:#ff0000;
       }
/* .toast-success{
 background :#006600;
} */

    .toast-success{background: #006600;}
    .toast-error{background: red;}
    .toast-warning{background: coral;}
    .toast-info{background: cornflowerblue;}
    .toast-question{background: grey;}
</style>

<div class="body flex-grow-1 px-3">
        <div class="container-lg">
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left" style="float: left;">
            <h2>Add New Invoice</h2>
        </div>
        <div class="pull-right" style="float: right;">
            <a class="btn btn-primary" href="{{ route('invoices.index') }}"> Back</a>
        </div>
    </div>
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form action="{{ route('invoices.store') }}" method="POST" id="invoiceDetails">
    @csrf
  
    <div class="row g-3">

        <div class="row g-3" style="justify-content: space-between;">
            <div class="col-sm-2">
                <label for="basic-url" class="form-label">Invoice Number</label>
                <input type="text" class="form-control form-control-sm" placeholder="Invoice Number" id="invoice_number" name="invoice_number">
            </div>
            <div class="col-sm-3">
                <label for="basic-url" class="form-label">Invoice Date</label>
                <input type="text" class="form-control form-control-sm" id="datepicker" placeholder="Invoice Date" name="invoice_date">
            </div>
            <div class="col-sm-3">
                <label for="basic-url" class="form-label">Select Customer</label>
                <input type="hidden" id="customer_id" name="customer_id" value=" ">
                <input type="text" class="form-control form-control-sm" id="search_customers" placeholder="Select Customer" data-parsley-required="true" data-parsley-error-message="Enter the Customer Name">
            </div>
        </div>


        <div class="row g-3" >
            
            <div class="col-sm">
                <label for="basic-url" class="form-label">Customer Name</label>
                <input type="text" class="form-control form-control-sm" id="customer_name" placeholder="Customer Name" name="customer_name" readonly>
            </div>
            <div class="col-sm">
                <label for="basic-url" class="form-label">Email</label>
                <input type="text" class="form-control form-control-sm" id="email" placeholder="Email" name="email" readonly>
            </div>
            <div class="col-sm">
                <label for="basic-url" class="form-label">Phone</label>
                <input type="text" class="form-control form-control-sm" id="phone" placeholder="Phone" name="phone" readonly>
            </div>
            
            
        <!-- </div>

        <div class="row g-3"> -->
            <div class="col-sm">
                <label for="basic-url" class="form-label">Address</label>
                <input type="text" class="form-control form-control-sm" id="address" placeholder="Address" name="address" readonly>
            </div>
            <div class="col-sm">
                <label for="basic-url" class="form-label">Zipcode</label>
                <input type="text" class="form-control form-control-sm" id="zipcode" placeholder="Zipcode" name="zipcode" readonly>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <div class="row g-3">
                    
                    <div class="row g-3" >
                        <div class="col-sm">
                            <label for="basic-url" class="form-label">Select Item</label>
                            <input type="hidden" id="product_id" name="product_id" value=" ">
                            <input type="text" class="form-control form-control-sm" id="search_product" placeholder="Select Item" >
                        </div>
                        <div class="col-sm">
                            <label for="basic-url" class="form-label">Quantity</label>
                            <input type="number" class="form-control form-control-sm" id="quantity" placeholder="Quantity" name="quantity"><span id="error"></span>
                            <input type="hidden" class="form-control form-control-sm" id="sku" placeholder="sku" name="sku">
                        </div>
                        <div class="col-sm">
                            <label for="basic-url" class="form-label">Price</label>
                            <input type="text" class="form-control form-control-sm" id="price" placeholder="Price" name="price" readonly>
                        </div>
                        <div class="col-sm">
                            <label for="basic-url" class="form-label">Total Price</label>
                            <input type="text" class="form-control form-control-sm" id="total_price" placeholder="Total Price" name="total_price" readonly>
                        </div>
                        
                        <div class="col-sm">
                            <label for="basic-url" class="form-label">Stock</label>
                            <input type="text" class="form-control form-control-sm" id="stock" placeholder="Stock" name="stock" readonly>
                        </div>
                        <div class="col-sm">
                            <label class="control-label" for="">&nbsp;</label><br>
                            <button class="btn btn-outline-info" style="padding: 2px 10px;" id="add_row">Add Row</button>
                        </div>
                        
                        
                    </div>
                </div></br>
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Select Item</th>
                            <th>SKU</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Total</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody class="add_item">
                       
                    </tbody>
                </table>
                <div class=" pull-right" style="float: right;">
                    <div class="form-group" id="box1">
                        <label for="Sub Total">Sub Total</label>
                        <input type="text" class="form-group" id="sub_total" name="sub_total" value="" readonly/>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row g-3">
            
        <div class="col-xs-4">
                <button type="submit" class="btn btn-info" id="createInvoice">Create Invoice</button>
        </div>
        </div>
    </div>

</form>



    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script> -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <script>
        $(function(){
          $("#datepicker").datepicker({
            minDate: 0,
            format: "mm-dd-yyyy",
            todayHighlight: true,
            startDate: today,
            endDate: end,
            autoclose: true

          });
          $('#datepicker').datepicker('setDate', today);
      });
            function Generator() {};
                Generator.prototype.rand =  Math.floor(Math.random() * 5) + Date.now();
                Generator.prototype.getId = function() {
                return this.rand++;
                };
                var idGen =new Generator();
                console.log("Invoice_no: ", idGen.rand);
                 $('#invoice_number').val(idGen.rand);


        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $(document).ready(function(){
            $("#search_customers").autocomplete({
                source: function(request, response){
                    $.ajax({
                        url: "{{route('getCustomers')}}",
                        type: "post",
                        datatype: "json",
                        data: {
                            _token: CSRF_TOKEN,
                            search: request.term
                        },
                        success: function(data){
                            response(data);
                        }
                    })
                },
                select: function(event, ui){
                    $("#search_customers").val(ui.item.label);
                    $("#customer_id").val(ui.item.value);
                    $("#customer_name").val(ui.item.label);
                    $("#email").val(ui.item.email);
                    $("#phone").val(ui.item.phone);
                    $("#address").val(ui.item.address);
                    $("#zipcode").val(ui.item.zipcode);
                    return false;
                }

            });



            $("#search_product").autocomplete({
                source: function(request, response){
                    $.ajax({
                        url: "{{route('getProducts')}}",
                        type: "post",
                        datatype: "json",
                        data: {
                            _token: CSRF_TOKEN,
                            search: request.term
                        },
                        success: function(data){
                            response(data);
                        }
                    })
                },
                select: function(event, ui){
                    $("#search_product").val(ui.item.label);
                    $("#product_id").val(ui.item.value);
                    $("#price").val(ui.item.price);
                    $("#sku").val(ui.item.sku);
                    $("#category").val(ui.item.category);
                    $("#stock").val(ui.item.stock);
                    return false;
                }

            });
        });

        $(document).on('keyup','#quantity',function(){


            var quantity=parseInt($(this).val());
            var price =parseInt($('#price').val());
            var stock =parseInt($('#stock').val());
            
            if(quantity <= stock){
                var total =(quantity*price);
                $("#total_price").val(total);
                $('#error').text("");
            }else{
                $('#quantity').val('');
                $("#total_price").val('');
                console.log("false");
                $('#error').text("Value Exceeded");
            }
            // console.log(typeof stock)
        });

        $(document).ready(function(){

            var add_new_item = $("#add_row");
            var x = 1;


                $(add_new_item).click(function(){
                    var id = $("#product_id").val();
                    var search_product = $("#search_product").val();
                    var added_sku = $("#sku").val();
                    var price = $("#price").val();
                    var quantity = $("#quantity").val();
                    var total_price = $("#total_price").val();
                    var appended_row = '<tr><td id="appended_row['+x+']"><input type="hidden" name="items['+x+'][product_id]" value="'+id+'" readonly><input type="text" id="search_product" name="items['+x+'][search_product]" value="'+search_product+'" readonly></td><td><input id="added_sku" name="items['+x+'][added_sku]" value="'+added_sku+'" readonly></td><td><input id="price" name="items['+x+'][price]" value="'+price+'" placeholder="Price per unit" readonly></td><td><input type="number" id="quantity" name="items['+x+'][quantity]" value="'+quantity+'" readonly></td><td><input type="text" class="item_total" id="items['+x+'][total_price]" name="items['+x+'][total_price]" value="'+total_price+'" readonly></td><td><button class="btn btn-danger remove_button">Del</button></td></tr>'
                    if(search_product!="" && quantity!=""){
                    x++;
                    $("tbody.add_item").append(appended_row);
                    grandTotal = 0;
                    $(".item_total").each(function(){
                        grandTotal += parseInt($(this).val());
                    console.log("Grand-Total:", grandTotal);
                    });

                    $("#sub_total").val(grandTotal);
                    $("#search_product").val("");
                    $("#sku").val("");
                    $("#stock").val("");
                    $("#price").val("");
                    $("#quantity").val("");
                    $("#total_price").val("");
                    
                    }else{
                        $('#error').text("Select Product");
                        //alert('fill the details first!');
                    }
                    return false;
                    
                });

                //Once remove button is clicked
                $("tbody.add_item").on('click', '.remove_button', function (e) {
                    e.preventDefault();
                    $(this).closest('tr').remove(); 

                }) ;
        })



        $(document).on('click', '#createInvoice', function(e){
          e.preventDefault();
          if ($('#invoiceDetails').parsley().validate()) {
          var data = $("#invoiceDetails").serialize();
          console.log(data);
          $.ajax({
                url: "{{ route('invoices.store') }}",
                type: "POST",
                data: data,
                dataType: "json",
                success: function (response) {
                  console.log("hello");
                  if(response="success"){
                  console.log('success');
                  toastr.options.onHidden = function() {
                      location.href ="{{ route('invoices.index') }}";
                      }
                    toastr.success("Invoice Created");

                  }else{
                    console.log("failed");
                  }
                
              }
                
              });
         }
        })
        
    </script>
    <script type="text/javascript">
            
        // Toast Type
            $('#success').click(function(event) {
                toastr.success('Record Added Successfully');
            });
            $('#info').click(function(event) {
                toastr.info('You clicked Info toast')
            });
            $('#error').click(function(event) {
                toastr.error('You clicked Error Toast')
            });
            $('#warning').click(function(event) {
                toastr.warning('You clicked Warning Toast')
            });

        // Toast Image and Progress Bar
            $('#image').click(function(event) {
                toastr.options.progressBar = true,
                toastr.info('<img src="https://image.flaticon.com/icons/svg/34/34579.svg" style="width:150px;">', 'Toast Image')
            });


        // Toast Position
            $('#position').click(function(event) {
                var pos = $('input[name=position]:checked', '#positionForm').val();
                toastr.options.positionClass = "toast-" + pos;
                toastr.options.preventDuplicates = false;
                toastr.info('This sample position', 'Toast Position')
            });
    </script>
</div>
</div>
@endsection